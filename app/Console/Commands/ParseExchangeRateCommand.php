<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\ExchangeRate\ExchangeRateServiceInterface;

class ParseExchangeRateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:rates:parse';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse exchange rates';

    /**
     * Execute the console command.
     */
    public function handle(ExchangeRateServiceInterface $service): void
    {
        $service->getCurrentRates();
    }
}
