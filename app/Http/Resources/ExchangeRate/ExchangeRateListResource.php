<?php

namespace App\Http\Resources\ExchangeRate;

use Illuminate\Http\Request;
use App\Services\ExchangeRate\DTO\ExchangeRateListDTO;
use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Attributes as OA;

/**
 * @mixin ExchangeRateListDTO
 */
#[OA\Schema(
    title: 'ExchangeRateListResource',
    properties: [
        new OA\Property(property: 'rates', type: 'array', items: new OA\Items(ref: '#/components/schemas/ExchangeRateItemDTO', type: 'object')),
        new OA\Property(property: 'parseDate', type: 'string', format: 'date-time', example: '2024-04-21T11:31:16.798298Z'),
        new OA\Property(property: 'ratesForDate', type: 'string', example: '2024-04-20'),
    ]
)]
class ExchangeRateListResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'rates' => $this->rates,
            'parseDate' => $this->parseDate,
            'ratesForDate' => $this->ratesForDate->format('Y-m-d'),
        ];
    }
}
