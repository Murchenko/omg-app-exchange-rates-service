<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ExternalServiceUnavailableException extends Exception
{
    public function render(Request $request): Response
    {
        return response([
            'message' => 'Unable to connect to external service. Please try again later.',
        ], 502);
    }
}
