<?php

namespace App\Services\ExchangeRate\DTO;

use App\DTO\BaseDTO;
use OpenApi\Attributes as OA;

#[OA\Schema(
    title: 'ExchangeRateItemDTO',
    properties: [
        new OA\Property(property: 'id', type: 'string', example: 'R01010'),
        new OA\Property(property: 'numCode', type: 'string', example: '036'),
        new OA\Property(property: 'charCode', type: 'string', example: 'USD'),
        new OA\Property(property: 'nominal', type: 'integer', example: 1),
        new OA\Property(property: 'name', type: 'string', example: 'Доллар США'),
        new OA\Property(property: 'value', type: 'number', example: 66.0000),
        new OA\Property(property: 'vUnitRate', type: 'number', example: 66.0000),
    ]
)]
class ExchangeRateItemDTO extends BaseDTO
{
    public string $id;

    public string $numCode;

    public string $charCode;

    public int $nominal;

    public string $name;

    public float $value;

    public float $vUnitRate;
}
