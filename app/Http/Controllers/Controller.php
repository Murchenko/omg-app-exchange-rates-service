<?php

namespace App\Http\Controllers;

use OpenApi\Attributes as OA;

#[OA\Info(version: '1.0', title: 'Exchange Rate Parser API')]
abstract class Controller
{
    //
}
