<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ExternalServiceUnexpectedResponseException extends Exception
{
    public function render(Request $request): Response
    {
        return response([
            'message' => 'Unexpected response from external service. Please try again later.',
        ], 502);
    }
}
