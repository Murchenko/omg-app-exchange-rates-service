<?php

return [
    'parse_url' => env('EXCHANGE_RATE_PARSE_URL'),
    'retry' => [
        'max' => env('EXCHANGE_RATE_PARSE_RETRY_MAX'),
        'delay' => env('EXCHANGE_RATE_PARSE_RETRY_DELAY'), // ms
    ],
    'cache_key' => 'exchange-rates-dto',
];
