# Тестовое задание для PHP Laravel

## Задача:

Ваша задача - создать микросервис на PHP с использованием Laravel, который взаимодействует с внешним REST API.
Микросервис должен быть отказоустойчивым и спроектирован так, чтобы обеспечивать надежную интеграцию с внешним сервисом.

## Software requirements

* PHP 8.3
* Laravel 11
* MySQL 8.3
* Redis 6

## Установка

* Скопировать .env.example переменные:</br>``` cp .env.example .env ```<br><br>
* При необходимости скорректировать окружение </br> ```nano .env```
* Установить зависимости:
    * Для локальной разработки:<br>``` composer install -o ```<br><br>
    * Для продакшена:<br>``` composer install --no-dev -o ```<br><br>

## Cron (scheduler)

Необходимо настройка laravel scheduler (https://laravel.com/docs/11.x/scheduling#running-the-scheduler) для запуска
следующих команд:

- `app:rates:parse` - парсинг курсов валют по расписанию

## Environments:

```
# EXCHANGE RATE PARSE URL
EXCHANGE_RATE_PARSE_URL="https://www.cbr.ru/scripts/XML_daily.asp" - URL для парсинга курсов 
# кол-во повторных попыток при неудачных запросах
EXCHANGE_RATE_PARSE_RETRY_MAX=3
# ожидание между попытками
EXCHANGE_RATE_PARSE_RETRY_DELAY=200
```

## Swagger (OpenAPI) doc

Для генерации документации используйте команду: `composer swagger`. Swagger файл будет в корне проекта с названием
openapi.yaml.

## Рекомендации к разработке

Перед каждым коммитом в гит рекомендуется использовать команду: `composer before-commit`
Данная команда запускает:

- тесты
- pint (php-cs-fixer)
- phpstan
- генерацию swagger документации
