<?php

namespace App\Services\ExchangeRate;

use App\Services\ExchangeRate\DTO\ExchangeRateListDTO;

interface ExchangeRateServiceInterface
{
    public function getCurrentRates(): ExchangeRateListDTO;

    public function parseRatesFromRemoteApi(): ExchangeRateListDTO;
}
