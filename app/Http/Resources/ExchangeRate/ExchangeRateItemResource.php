<?php

namespace App\Http\Resources\ExchangeRate;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Services\ExchangeRate\DTO\ExchangeRateItemDTO;

/**
 * @mixin ExchangeRateItemDTO
 */
class ExchangeRateItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'numCode' => $this->numCode,
            'charCode' => $this->charCode,
            'nominal' => $this->nominal,
            'name' => $this->name,
            'value' => $this->value,
            'vUnitRate' => $this->vUnitRate,
        ];
    }
}
