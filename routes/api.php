<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ExchangeRateController;

Route::get('/exchange-rates', [ExchangeRateController::class, 'index'])->name('exchange-rates.index');
