<?php

namespace App\Services\ExchangeRate;

use Illuminate\Support\Arr;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\Client\RequestException;
use Illuminate\Http\Client\ConnectionException;
use App\Services\ExchangeRate\DTO\ExchangeRateListDTO;
use App\Services\ExchangeRate\DTO\ExchangeRateItemDTO;
use App\Exceptions\ExternalServiceUnavailableException;
use App\Exceptions\ExternalServiceUnexpectedResponseException;

class ExchangeRateService implements ExchangeRateServiceInterface
{
    public function getCurrentRates(): ExchangeRateListDTO
    {
        return Cache::remember(config('exchange-rate-parser.cache_key'), now()->endOfDay(), function () {
            return $this->parseRatesFromRemoteApi();
        });
    }

    /**
     * @throws ExternalServiceUnavailableException|ExternalServiceUnexpectedResponseException
     */
    public function parseRatesFromRemoteApi(): ExchangeRateListDTO
    {
        try {
            $res = Http::retry(
                config('exchange-rate-parser.retry.max'),
                config('exchange-rate-parser.retry.delay')
            )
                ->get(config('exchange-rate-parser.parse_url'))
                ->throwUnlessStatus(200)
                ->body();
        } catch (ConnectionException|RequestException) {
            throw new ExternalServiceUnavailableException();
        }

        $this->validateResponse($res);

        return $this->responseToDTO($res);
    }

    private function xmlToJson(string $xml): array
    {
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);

        return json_decode($json, true);
    }

    private function isXml(string $value): bool
    {
        $prev = libxml_use_internal_errors(true);

        $doc = simplexml_load_string($value);
        $errors = libxml_get_errors();

        libxml_clear_errors();
        libxml_use_internal_errors($prev);

        return $doc !== false && empty($errors);
    }

    /**
     * @throws ExternalServiceUnexpectedResponseException
     */
    private function validateResponse(string $resXml): void
    {
        $isXml = $this->isXml($resXml);
        if (! $isXml) {
            throw new ExternalServiceUnexpectedResponseException();
        }

        $json = $this->xmlToJson($resXml);
        if (
            Arr::get($json, 'Valute') === null ||
            Arr::get($json, '@attributes.Date') === null ||
            Arr::get($json, 'Valute.0') === null
        ) {
            throw new ExternalServiceUnexpectedResponseException();
        }

        // TODO: ну можно еще в Valute каждый ключ проверить
    }

    private function responseToDTO(string $res): ExchangeRateListDTO
    {
        $json = $this->xmlToJson($res);
        $ratesJson = $json['Valute'];

        $rates = Arr::map($ratesJson, function (array $cbRate) {
            return ExchangeRateItemDTO::from([
                'id' => $cbRate['@attributes']['ID'],
                'numCode' => $cbRate['NumCode'],
                'charCode' => $cbRate['CharCode'],
                'nominal' => intval($cbRate['Nominal']),
                'name' => $cbRate['Name'],
                // TODO: можно вынести в отдельную функцию
                'value' => floatval(str_replace(',', '.', $cbRate['Value'])),
                'vUnitRate' => floatval(str_replace(',', '.', $cbRate['VunitRate'])),
            ]);
        });

        return ExchangeRateListDTO::from([
            'rates' => $rates,
            'parseDate' => now(),
            'ratesForDate' => Carbon::parse($json['@attributes']['Date']),
        ]);
    }
}
