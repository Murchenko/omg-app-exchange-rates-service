<?php

use Illuminate\Support\Facades\Schedule;
use App\Console\Commands\ParseExchangeRateCommand;

Schedule::command(ParseExchangeRateCommand::class)->everyTenMinutes();
