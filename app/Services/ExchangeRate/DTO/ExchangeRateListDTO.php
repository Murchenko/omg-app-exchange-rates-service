<?php

namespace App\Services\ExchangeRate\DTO;

use App\DTO\BaseDTO;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

class ExchangeRateListDTO extends BaseDTO
{
    /** @var Collection<int, ExchangeRateItemDTO> */
    public Collection $rates;

    public Carbon $parseDate;

    public Carbon $ratesForDate;
}
