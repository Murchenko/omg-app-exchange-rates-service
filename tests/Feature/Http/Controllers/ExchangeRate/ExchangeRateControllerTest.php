<?php

namespace Http\Controllers\ExchangeRate;

use Tests\TestCase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Testing\TestResponse;
use Illuminate\Support\Facades\Cache;
use App\Services\ExchangeRate\DTO\ExchangeRateListDTO;
use App\Services\ExchangeRate\DTO\ExchangeRateItemDTO;

class ExchangeRateControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     */
    public function test_without_cache(): void
    {
        $externalServiceResponse = file_get_contents('mock/external-service-response.xml', true);

        Http::fake([
            config('app.exchange-rate-parser.parse_url') => Http::response($externalServiceResponse, headers: [
                'Content-Type' => 'application/xml',
            ]),
        ]);

        $res = $this
            ->get(route('exchange-rates.index'))
            ->assertOk();

        $this->assertIndexRatesJsonStructure($res);

        /** @var ExchangeRateListDTO|null|mixed $cacheRates */
        $cacheRates = Cache::get(config('exchange-rate-parser.cache_key'));

        $this->assertTrue($cacheRates !== null);
        $this->assertTrue($cacheRates instanceof ExchangeRateListDTO);

        // TODO: можно распарсить XML файл и пройтись циклом и проверить что он верно сохранил каждый курс
    }

    public function test_with_cache(): void
    {
        Http::fake();

        $fakeDTO = $this->generateFakeRatesDTO();

        Cache::set(config('exchange-rate-parser.cache_key'), $fakeDTO);

        $res = $this->get(route('exchange-rates.index'))
            ->assertOk();

        $this->assertIndexRatesJsonStructure($res);

        $res->assertJsonPath('parseDate', $fakeDTO->parseDate->toISOString());
        $res->assertJsonPath('ratesForDate', $fakeDTO->ratesForDate->format('Y-m-d'));
        $res->assertJsonPath('rates.0.id', $fakeDTO->rates[0]->id);
        $res->assertJsonPath('rates.0.value', $fakeDTO->rates[0]->value);
        $res->assertJsonPath('rates.0.numCode', $fakeDTO->rates[0]->numCode);
    }

    private function assertIndexRatesJsonStructure(TestResponse $res): TestResponse
    {
        return $res
            ->assertJsonStructure([
                'rates' => [
                    '*' => [
                        'id',
                        'numCode',
                        'charCode',
                        'nominal',
                        'name',
                        'value',
                        'vUnitRate',
                    ],
                ],
                'parseDate',
                'ratesForDate',
            ]);
    }

    private function generateFakeRatesDTO(): ExchangeRateListDTO
    {
        return ExchangeRateListDTO::from([
            'rates' => [
                ExchangeRateItemDTO::from([
                    'id' => 'R01720',
                    'numCode' => '980',
                    'charCode' => 'UAH',
                    'nominal' => 10,
                    'name' => 'Украинских гривен',
                    'value' => 23.5940,
                    'vUnitRate' => 2.3594,
                ]),
            ],
            'parseDate' => Carbon::now(),
            'ratesForDate' => Carbon::now(),
        ]);
    }

    public function test_response_html()
    {
        Http::fake([
            config('app.exchange-rate-parser.parse_url') => Http::response(fake()->randomHtml()),
        ]);

        $this
            ->get(route('exchange-rates.index'))
            ->assertStatus(502);
    }

    public function test_response_not_ok()
    {
        Http::fake([
            config('app.exchange-rate-parser.parse_url') => Http::response(null, 400),
        ]);

        $this
            ->get(route('exchange-rates.index'))
            ->assertStatus(502);
    }

    public function test_response_unexpected_content()
    {
        $externalServiceInvalidResponse = file_get_contents('mock/external-service-invalid-response.xml', true);

        Http::fake([
            config('app.exchange-rate-parser.parse_url') => Http::response($externalServiceInvalidResponse, headers: [
                'Content-Type' => 'application/xml',
            ]),
        ]);

        $this
            ->get(route('exchange-rates.index'))
            ->assertStatus(502);
    }

    // TODO: тесты при плохом соединение с сервером (timeout)
}
