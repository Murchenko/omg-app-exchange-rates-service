<?php

namespace App\DTO;

use Spatie\LaravelData\Data;
use Spatie\LaravelData\Optional;

class BaseDTO extends Data
{
    public function isOptional(mixed $param): bool
    {
        return $param instanceof Optional;
    }

    public function isNotOptional(mixed $param): bool
    {
        return ! $this->isOptional($param);
    }
}
