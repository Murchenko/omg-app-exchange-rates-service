<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Services\ExchangeRate\ExchangeRateServiceInterface;
use App\Http\Resources\ExchangeRate\ExchangeRateListResource;
use OpenApi\Attributes as OA;

class ExchangeRateController extends Controller
{
    public function __construct(
        protected readonly ExchangeRateServiceInterface $exchangeRateParserService
    ) {
    }

    #[OA\Get(
        path: '/exchange-rates',
        operationId: 'IndexExchangeRate',
        summary: 'Get exchange rates list',
        tags: ['exchange-rates'],
        responses: [
            new OA\Response(
                response: '200',
                description: '',
                content: new OA\JsonContent(ref: '#/components/schemas/ExchangeRateListResource', type: 'object')
            ),
        ]
    )]
    public function index(): JsonResponse
    {
        $res = $this->exchangeRateParserService->getCurrentRates();

        return new JsonResponse(ExchangeRateListResource::make($res));
    }
}
